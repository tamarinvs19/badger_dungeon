import 'package:badger_dungeon/badger_dungeon.dart';
import 'package:badger_dungeon/managers/directions.dart';
import 'package:badger_dungeon/objects/base_block.dart';
import 'package:badger_dungeon/objects/eye.dart';
import 'package:badger_dungeon/objects/portal_block.dart';
import 'package:badger_dungeon/objects/stone_block.dart';
import 'package:flame/collisions.dart';
import 'package:flame/components.dart';

class BadgerPlayer extends SpriteAnimationComponent
    with CollisionCallbacks, HasGameRef<BadgerDungeonGame> {
  BadgerPlayer({
    required super.position,
  }) : super(size: Vector2.all(64), anchor: Anchor.center);

  bool reversed = false;
  final double _speed = 200.0;
  Vector2 velocity = Vector2.zero();

  double outOfMapDistance = 96;

  Direction direction = Direction.none;

  @override
  Future<void> onLoad() async {
    animation = SpriteAnimation.fromFrameData(
        game.images.fromCache("badger.png"),
        SpriteAnimationData.sequenced(
            amount: 1,
            stepTime: 0.12,
            textureSize: Vector2.all(64),
        ),
    );

    add(
      CircleHitbox(),
    );
  }

  @override
  void update(double dt) async {
    velocity = moveBadger(dt);
    game.velocity = Vector2.zero();

    Vector2 nextPosition = position + velocity * dt;
    if (nextPosition.x < outOfMapDistance) {
      game.velocity = Vector2(game.size.x / 2, 0);
    }
    if (nextPosition.x > game.size.x - outOfMapDistance) {
      game.velocity = Vector2(-game.size.x / 2, 0);
    }
    if (nextPosition.y < outOfMapDistance) {
      game.velocity = Vector2(0, game.size.y / 2);
    }
    if (nextPosition.y > game.size.y - outOfMapDistance) {
      game.velocity = Vector2(0, -game.size.y / 2);
    }

    position += (velocity + game.velocity) * dt;

    super.update(dt);
  }

  @override
  void flipHorizontally() => transform.flipHorizontally();

  Vector2 moveBadger(double dt) {
    if (game.gameOver) {
      return Vector2.zero();
    }

    int dx = 0;
    int dy = 0;

    switch (direction) {
      case Direction.up:
        dy = -1;
        break;
      case Direction.down:
        dy = 1;
        break;
      case Direction.left:
        dx = -1;
        if (reversed) {
          reversed = false;
          flipHorizontally();
        }
        break;
      case Direction.right:
        dx = 1;
        if (!reversed) {
          reversed = true;
          flipHorizontally();
        }
        break;
      case Direction.none:
        break;
    }
    return Vector2(_speed * dx, _speed * dy);
  }

  @override
  void onCollision(Set<Vector2> intersectionPoints, PositionComponent other) {
    if (other is BaseBlock) {
      other.openBlock();
    }

    if (other is StoneBlock || (other is PortalBlock && game.eyeCount < 9)) {
      if (intersectionPoints.length == 2) {
        final mid = (intersectionPoints.elementAt(0) +
            intersectionPoints.elementAt(1)) /
            2;

        final collisionNormal = absoluteCenter - mid;
        final separationDistance = (size.x / 2) - collisionNormal.length;
        collisionNormal.normalize();

        position += collisionNormal.scaled(separationDistance);
      }
    }

    if (other is PortalBlock && game.eyeCount == 9) {
      game.finishTime = DateTime.now();
      game.gameOver = true;
    }

    if (other is Eye) {
      other.removeFromParent();
      game.eyeCount += 1;
    }

    super.onCollision(intersectionPoints, other);
  }
}