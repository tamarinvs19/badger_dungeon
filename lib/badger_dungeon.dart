import 'dart:async';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';

import 'package:badger_dungeon/managers/map/map.dart';
import 'package:badger_dungeon/objects/eye.dart';
import 'package:badger_dungeon/objects/portal_block.dart';
import 'package:badger_dungeon/overlays/eye.dart';

import 'package:badger_dungeon/actors/badger.dart';
import 'package:badger_dungeon/objects/soil_block.dart';
import 'package:badger_dungeon/objects/stone_block.dart';

import 'managers/directions.dart';
import 'managers/segment_manager.dart';

class BadgerDungeonGame extends FlameGame with HasCollisionDetection {
  int fieldSize;
  BadgerDungeonGame(this.fieldSize);

  Vector2 velocity = Vector2.zero();

  late BadgerPlayer _badger;

  bool gameOver = false;
  int eyeCount = 0;
  late DateTime startTime;
  late DateTime finishTime;

  void onDirectionChanged(Direction direction) {
    _badger.direction = direction;
  }

  @override
  Future<void> onLoad() async {
    await images.loadAll([
      'badger.png',
      'eye.png',
      'eye_hidden.png',
      'stone.png',
      'portal.png',
      'soil_broken.png',
      'unknown_block.png',
    ]);
    initializeGame(true);
  }

  @override
  Color backgroundColor() {
    return const Color.fromARGB(20, 173, 223, 247);
  }

  @override
  void update(double dt) {
    if (gameOver) {
      overlays.add('GameOver');
    }
    super.update(dt);
  }

  GameMap createMap() {
    return GameMap(MapGenerator(fieldSize).generateBox());
  }

  void clear() {
    _badger.removeFromParent();
    for (var element in children) {element.removeFromParent();}
  }

  void initializeGame(bool loadEye) {
    List<FieldBlock> segment = createMap().blocks;
    for (final block in segment) {
      switch (block.blockType) {
        case SoilBlock:
          add(
            SoilBlock(gridPosition: block.gridPosition)
          );
          break;
        case StoneBlock:
          add(
            StoneBlock(gridPosition: block.gridPosition)
          );
          break;
        case PortalBlock:
          add(
              PortalBlock(gridPosition: block.gridPosition)
          );
          break;
        case Eye:
          var eye = Eye(gridPosition: block.gridPosition);
          add(eye);
          break;
      }
    }

    _badger = BadgerPlayer(position: Vector2(canvasSize.x / 2, canvasSize.y / 2));
    add(_badger);
    if (loadEye) {
      add(EyeComponent());
    }
    startTime = DateTime.now();
  }

  void reset() {
    clear();
    gameOver = false;
    var timer = Timer(const Duration(seconds: 5), () { });
    timer.cancel();
    eyeCount = 0;
    initializeGame(true);
  }
}