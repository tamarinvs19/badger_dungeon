import 'package:flame/game.dart';
import 'package:flutter/material.dart';

import 'badger_dungeon.dart';
import 'managers/handlebar.dart';
import 'overlays/game_over.dart';
import 'overlays/main_menu.dart';

class MainGamePage extends StatefulWidget {
  const MainGamePage({Key? key}) : super(key: key);

  @override
  MainGameState createState() => MainGameState();
}

class MainGameState extends State<MainGamePage> {
  BadgerDungeonGame game = BadgerDungeonGame(20);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color.fromRGBO(0, 0, 0, 1),
        body: Stack(
          children: [
            GameWidget(
              game: game,
              overlayBuilderMap: {
                'MainMenu': (_, BadgerDungeonGame game) => MainMenu(game: game),
                'GameOver': (_, BadgerDungeonGame game) => GameOver(game: game),
              },
              initialActiveOverlays: const ['MainMenu'],
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Padding(
                padding: const EdgeInsets.all(32.0),
                child:
                Handlebar(onDirectionChanged: game.onDirectionChanged),
              ),
            )
          ],
        ));
  }
}
