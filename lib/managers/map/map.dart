import 'dart:math';

import 'package:badger_dungeon/objects/eye.dart';
import 'package:badger_dungeon/objects/portal_block.dart';
import 'package:badger_dungeon/objects/soil_block.dart';
import 'package:flame/components.dart';

import '../../objects/stone_block.dart';
import '../segment_manager.dart';

class GameMap {
  List<FieldBlock> blocks;
  GameMap(this.blocks);
}

class MapGenerator {
  int mapSize;
  MapGenerator(this.mapSize);

  List<FieldBlock> generate() {
    return [];
  }

  List<FieldBlock> generateBox() {
    List<FieldBlock> blocks = [];
    for (int i = 0; i <= mapSize; i++) {
      blocks.add(FieldBlock(Vector2(i.toDouble(), 0), StoneBlock));
      blocks.add(FieldBlock(Vector2(0, i.toDouble() + 1), StoneBlock));
      blocks.add(FieldBlock(Vector2(i.toDouble(), mapSize + 1), StoneBlock));
      blocks.add(FieldBlock(Vector2(mapSize + 1, i.toDouble()), StoneBlock));
    }
    for (int i = 1; i <= mapSize; i++) {
      for (int j = 1; j <= mapSize; j++) {
        blocks.add(FieldBlock(Vector2(i.toDouble(), j.toDouble()), SoilBlock));
      }
    }
    blocks = addPortal(blocks);
    blocks = addStone(blocks, 3 * mapSize - 1);
    blocks = addEye(blocks);
    return blocks;
  }

  List<FieldBlock> addPortal(List<FieldBlock> blocks) {
    var random = Random();
    int index = random.nextInt(blocks.length);
    blocks[index] = FieldBlock(blocks[index].gridPosition, PortalBlock);
    return blocks;
  }

  List<FieldBlock> addEye(List<FieldBlock> blocks) {
    var random = Random();
    List<Vector2> eyePositions = [];
    while (eyePositions.length < 9) {
      int index = random.nextInt(blocks.length);
      FieldBlock block = blocks[index];
      if (!eyePositions.contains(block.gridPosition) && block.blockType == SoilBlock) {
        eyePositions.add(block.gridPosition);
      }
    }
    for (var value in eyePositions) {
      blocks.add(FieldBlock(value, Eye));
    }
    return blocks;
  }

  List<FieldBlock> addStone(List<FieldBlock> blocks, int count) {
    var random = Random();
    while (count > 0) {
      int index = random.nextInt(blocks.length);
      FieldBlock block = blocks[index];
      if (block.blockType == SoilBlock) {
        blocks[index] = FieldBlock(block.gridPosition, StoneBlock);
        count--;
      }
    }
    return blocks;
  }

}
