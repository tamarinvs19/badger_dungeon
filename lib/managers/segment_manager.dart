import 'package:flame/components.dart';

class FieldBlock {
  final Vector2 gridPosition;
  final Type blockType;
  FieldBlock(this.gridPosition, this.blockType);
}
