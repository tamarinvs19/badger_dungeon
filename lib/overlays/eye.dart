import 'package:flame/components.dart';
import 'package:flutter/material.dart';

import 'package:badger_dungeon/badger_dungeon.dart';

class EyeComponent extends PositionComponent with HasGameRef<BadgerDungeonGame> {
  EyeComponent({
    super.position,
    super.size,
    super.scale,
    super.angle,
    super.anchor,
    super.children,
    super.priority = 0
  }) {
    positionType = PositionType.viewport;
  }

  late TextComponent _eyeCounter;

  @override
  Future<void>? onLoad() async {
    _eyeCounter = TextComponent(
      text: '${game.eyeCount}',
      textRenderer: TextPaint(
        style: const TextStyle(
          fontSize: 32,
          color: Color.fromRGBO(200, 90, 20, 1),
        ),
      ),
      anchor: Anchor.center,
      position: Vector2(game.size.x - 20, 20),
    );
    add(_eyeCounter);

    final eyeSprite = await game.loadSprite('eye.png');
    add(
      SpriteComponent(
        sprite: eyeSprite,
        position: Vector2(game.size.x - 60, 20),
        size: Vector2.all(32),
        anchor: Anchor.center,
      ),
    );
  }

  @override
  void update(double dt) {
    _eyeCounter.text = '${game.eyeCount}';
    super.update(dt);
  }
}