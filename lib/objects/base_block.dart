import 'package:badger_dungeon/badger_dungeon.dart';
import 'package:flame/collisions.dart';
import 'package:flame/components.dart';

class BaseBlock extends SpriteComponent with HasGameRef<BadgerDungeonGame> {
  final Vector2 gridPosition;
  String initImageName = "unknown_block.png";
  String realImageName;

  Vector2 velocity = Vector2.zero();

  bool _isHidden = true;

  BaseBlock({
    required this.gridPosition,
    required this.realImageName,
  }) : super(size: Vector2.all(64), anchor: Anchor.topLeft);

  @override
  Future<void> onLoad() async {
    final blockImage = game.images.fromCache(initImageName);
    sprite = Sprite(blockImage);
    position = Vector2(gridPosition.x * size.x, gridPosition.y * size.y);
    add(RectangleHitbox()..collisionType = CollisionType.passive);
  }

  void openBlock() async {
    if (_isHidden) {
      final blockImage = game.images.fromCache(realImageName);
      sprite = Sprite(blockImage);
      _isHidden = false;
    }
  }

  @override
  void update(double dt) {
    velocity = game.velocity;
    position += velocity * dt;
  }
}
