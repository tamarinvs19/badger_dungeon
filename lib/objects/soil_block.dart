import 'package:badger_dungeon/objects/base_block.dart';

class SoilBlock extends BaseBlock {
  SoilBlock({
    required super.gridPosition,
  }) : super(
    realImageName: "soil_broken.png"
  );
}