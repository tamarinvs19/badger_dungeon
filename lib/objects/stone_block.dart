import 'package:badger_dungeon/objects/base_block.dart';

class StoneBlock extends BaseBlock {
  StoneBlock({
    required super.gridPosition,
  }) : super(
      realImageName: "stone.png"
  );
}
