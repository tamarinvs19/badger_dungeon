import 'package:badger_dungeon/objects/base_block.dart';

class PortalBlock extends BaseBlock {
  PortalBlock({
    required super.gridPosition,
  }) : super(
      realImageName: "portal.png"
  );
}
