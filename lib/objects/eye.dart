import 'package:flame/collisions.dart';
import 'package:flame/components.dart';
import 'package:flame/effects.dart';
import 'package:flutter/widgets.dart';

import '../badger_dungeon.dart';

class Eye extends SpriteComponent with HasGameRef<BadgerDungeonGame> {
  final Vector2 gridPosition;

  Vector2 velocity = Vector2.zero();

  Eye({
    required this.gridPosition,
  }) : super(size: Vector2.all(32), anchor: Anchor.center);

  @override
  Future<void> onLoad() async {
    final eyeImage = game.images.fromCache("eye.png");
    sprite = Sprite(eyeImage);
    position = Vector2(
      (gridPosition.x * 64) + (size.x / 2) + 18,
      (gridPosition.y * 64) + (size.y / 2) + 16,
    );
    add(CircleHitbox()..collisionType = CollisionType.passive);
    add(
      SizeEffect.by(
          Vector2(-8, -8),
          EffectController(
            duration: .75,
            reverseDuration: .5,
            infinite: true,
            curve: Curves.easeOut,
          ),
      ),
    );
  }

  @override
  void update(double dt) {
    velocity = game.velocity;
    position += velocity * dt;
    super.update(dt);
  }
}